const _ = require('lodash');
const AWS = require('aws-sdk');

// load env
require('./test_config')();

const isTableExist = (tableName, listTables) => {
  if (!listTables) return false;
  return _.includes(listTables.TableNames, tableName)
};

const createTableIfNotExist = (tableName = process.env.app__DYNAMODB_TABLE) => {
  const options = {
    apiVersion: '2012-08-10',
    region: 'ap-southeast-1',
    endpoint: process.env.app__DynamoDb_endpoint,
    accessKeyId: process.env.app__access_key_id,
    secretAccessKey: process.env.app__access_key_secret,
  };

  console.log('---- options:', options);

  const dynamoDb = new AWS.DynamoDB(options);

  dynamoDb.listTables({
    ExclusiveStartTableName: 'STRING_VALUE'

  }, (err, data) => {

    if (err) return console.error(err);

    if (!isTableExist(tableName, data)) {
      dynamoDb.createTable({
        AttributeDefinitions: [
          {AttributeName: 'pk', AttributeType: 'S'},
          {AttributeName: 'id', AttributeType: 'S'}
        ],
        KeySchema: [
          {AttributeName: 'pk', KeyType: 'HASH'},
          {AttributeName: 'id', KeyType: 'RANGE'}
        ],
        ProvisionedThroughput: {
          ReadCapacityUnits: 10,
          WriteCapacityUnits: 5
        },
        TableName: tableName
      }, (err, res) => {
        if (err) return console.error(err);
        console.log(res);
      });

    } else {
      console.log(data);

    }
  });

};

createTableIfNotExist();
