const dotenv = require('dotenv');

/**
 * import this in front main file
 */
const loadConfigF = () => {
  // configure logging
  console.log('');
  console.log('NODE_ENV =', process.env.NODE_ENV);

  // load .env
  dotenv.config();

  // trace env
  Object.keys(process.env)
    .filter(k => /^app__/.test(k))
    .forEach(k => {
      console.log('%s = %s', k, process.env[k]);
    });
};

module.exports = () => {
  loadConfigF();
};
