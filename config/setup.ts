import pino from 'pino';
import '../src/utils/LoadConfig';

import {lambda} from "../src/graphql/GraphqlSchema";

const logger = pino({
  name: 'setup',
  level: 'debug',
});

function setup() {
  logger.debug(lambda.options);
}

try {
  setup();
  logger.info('---- setup done');

} catch (err) {
  logger.error(err);

}
