// load env
import './utils/LoadConfig';
import {lambda} from './graphql/GraphqlSchema';

// noinspection JSUnusedGlobalSymbols
export const graphql = lambda.graphqlHandler;
