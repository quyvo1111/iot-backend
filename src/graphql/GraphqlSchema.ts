import {isEmpty} from 'lodash';
import {logger} from '../utils/LogConfig';
import {GraphQLServerLambda} from 'graphql-yoga';

import {ConfigType} from './type/ConfigType';
import {Profile} from './type/Profile';
import {User} from './type/User';
import {generateGraphQlTypes} from '../utils/generate-schema';
import {Object} from 'aws-sdk/clients/s3';
import { SensorDetail } from './type/SensorDetail';
import {SensorData} from "./type/SensorData";

const defTypes = [
  ConfigType,
  User,
  Profile,
  SensorDetail,
  SensorData
];

logger.debug('---- initialize graphql schema - type count:', defTypes.length);

const schema = {
  typeDefs: [
    ...defTypes.map(t => t.typeDefs),
    `type Query {
      ${defTypes.filter(t => !isEmpty(t.query))
      .map(t => t.query['graphql'])
      .reduce((accumulator, currentValue) => (accumulator + '\n' + currentValue), '')}
    }`,
    `type Mutation {
      ${defTypes.filter(t => !isEmpty(t.mutation))
      .map(t => t.mutation.graphql).reduce((accumulator, currentValue) => (accumulator + '\n' + currentValue), '')}
    }`,
    `schema {
      query: Query
      mutation: Mutation
     }`
  ],

  resolvers: Object.assign(
    {},
    ...defTypes.filter(t => !isEmpty(t.resolvers)).map(t => t.resolvers),
    {
      Query: Object.assign(
        {},
        ...defTypes.filter(t => !isEmpty(t.query)).map(t => t.query['resolvers']),
      )
    },

    {
      Mutation: Object.assign(
        {},
        ...defTypes.filter(t => !isEmpty(t.mutation)).map(t => t.mutation.resolvers),
      )
    }
  )
};

export const lambda = new GraphQLServerLambda({
  typeDefs: schema.typeDefs.join(''),
  resolvers: schema.resolvers,
  context: async (ctx) => {
    const event = ctx['event'] || {};
    const context = ctx['context'] || {};

    // logger.debug('---- event:', event);
    // logger.debug('---- context:', context);
    // logger.debug('---- event.path:', event.path);
    // logger.debug('---- event.pathParameters:', event.pathParameters);
    // logger.debug('---- event.queryStringParameters:', event.queryStringParameters);

    return context;
  }
});

if (process.env.NODE_ENV !== 'production') {
  generateGraphQlTypes(lambda.executableSchema);
}
