/**
 * Created by manhvu on 12/14/17.
 */
export const ConfigType = {
  typeDefs: `
    type PageInfo {
      total: Int
      totalPages: Int
      page: Int
      size: Int
      isFirst: Boolean
      isLast: Boolean
    }
    
    input PageInput {
      page: Int!
      size: Int!
      sort: [String]
    }
    
    type PageScrollInfo {
      # next cursor is the highest id of the result plus 1
      nextCursor: ID
      size: Int
      isLast: Boolean
    }
    
    input PageScrollInput {
      # cursor is the highest id of the previous result plus 1
      cursor: ID
      size: Int!
      sort: [String]
    }
  `,

  resolvers: {},

  query: {
    graphql: `
      checkQuery: String 
    `,
    resolvers: {
      checkQuery: () => {
        return 'checkQuery ok!';
      }
    }
  },

  mutation: {
    graphql: `
      checkMutation: String
    `,
    resolvers: {
      checkMutation: () => {
        return 'checkMutation ok!';
      }
    }
  }
};
