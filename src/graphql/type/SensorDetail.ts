import {iSensor} from '../../service';

export const SensorDetail = {
  typeDefs: `

    type SensorDetail {
      owner: String
      type: String
      address: String
      description: String
      start_time: String
    }

  `,

  resolvers: {
  },

  query: {
    graphql: `
      sensorDetail(id: ID!): SensorDetail
      getSensorDetailByOwner(owner: String!): [SensorDetail]
    `,
    resolvers: {
      sensorDetail: (p, {id}, context, info) => {
        return iSensor.getSensorDetail(id, context);
      },
      getSensorDetailByOwner: async (p, {owner}, context, info) => {
        return await iSensor.getSensorDetailByOwner(owner, context);
      }

    }
  },

  mutation: {
    graphql: `
    `,
    resolvers: {
    }
  }
};
