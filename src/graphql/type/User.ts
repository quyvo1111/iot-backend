/**
 * Created by manhvu on 12/14/17.
 */
import {iCanService} from "../../service";

export const User = {
  typeDefs: `
    type CanKey {
      public_key: String!
      private_key: String
    }
    
    type CanAccountInfo {
      canAccount: String!
      key_active: CanKey!
      key_owner: CanKey!
    }
    
    type MyEmail {
      email: String
      isPrimary: Boolean
      verified: Boolean
      canAccounts: [CanAccountInfo]
    }
    
    type User {
      # equal to Username in cognito service
      id: ID
      name: String
      image: String
      email: String
      gender: String
      myEmails: [MyEmail]
    }
    
    type UserFilter {
      content: [User]
      pageInfo: PageInfo
      PaginationToken: String
    }
    
    type Recipient {
      email: ID
      Username: String
      isPrimary: Boolean
      dAppId: String
    }
    
    type RecipientFilter {
      content: [Recipient]
      pageScrollInfo: PageScrollInfo
    }
  `,

  resolvers: {},

  query: {
    graphql: ``,
    resolvers: {}
  },
  mutation: {
    graphql: `
      createCanAccount(canAccount: String!, OwnerKey: String!, ActiveKey: String!): Boolean 
    `,
    resolvers: {
      createCanAccount: async (p, {canAccount, OwnerKey, ActiveKey}, context) => {
        return await iCanService.createCanAccount(canAccount, OwnerKey, ActiveKey);
      }
    }
  }
};
