import {iSensorData} from '../../service';

export const SensorData = {
  typeDefs: `

    type data {
      type: String
      value: Int
      timestamp: String
      owner: String
    }

  `,

  resolvers: {
    Profile: {
    }
  },

  query: {
    graphql: `
      SensorData(from: String!, size: Int!, owner: String!, type: String!): [data]
      RecentData(size: Int!, owner: String!, type: String!): [data]
    `,
    resolvers: {
      SensorData: async (p, {from, size, owner, type}, context, info) => {
        return await iSensorData.getSensorData(from, size, owner, type, context);
      },
      RecentData: async (p, {size, owner, type}, context, info) => {
        return await iSensorData.getRecentData(size, owner, type);
      }
    }
  },

  mutation: {
    graphql: `
    `,
    resolvers: {
    }
  }
};
