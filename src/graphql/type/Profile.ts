/**
 * Created by manhvu on 12/14/17.
 */
import {iProfile} from '../../service';

export const Profile = {
  typeDefs: `

    type Profile {
      owner: String
      name: String
      email: String!
      address: String
      company: String
      telephone: String
      description: String
    }

  `,

  resolvers: {
    Profile: {
    }
  },

  query: {
    graphql: `
      profile(id: ID!): Profile
      testQuery(testV: String!): String
    `,
    resolvers: {
      profile: (p, {id}, context, info) => {
        return iProfile.getProfile(id, context);
      },
      testQuery: (p, {testV}, context, info) => {
        return "OK " + testV;
      }
    }
  },

  mutation: {
    graphql: `
    `,
    resolvers: {
    }
  }
};
