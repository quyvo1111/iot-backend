// load env
import './utils/LoadConfig';
import {GraphQLServer} from 'graphql-yoga';
import {lambda} from './graphql/GraphqlSchema';
import {appDemuxF} from './demux';
import {logger} from './utils/LogConfig';
import express from "express";
import bodyParser = require("body-parser");
import {iCanService} from "./service";

const graphQlServer = new GraphQLServer({
  schema: lambda.executableSchema
});

const IoT_Device = express();

IoT_Device.use(bodyParser.json());
IoT_Device.use(bodyParser.urlencoded({ extended: true }));

IoT_Device.post('/updateData', (req, res) => {
  console.log(req.body.data);
  let saveSensorDatas = [];
  try {

      saveSensorDatas = req.body.data.map((data) => {return {eos_account: req.body.eos_account,
                                                               private_key: req.body.private_key,
                                                                type: data.type,
                                                                value: data.value,
                                                                  };
      });
  }
  catch (e) {
      console.log('error when parse update data', e);
      res.send(e);
  }

  for (let i = 0; i < saveSensorDatas.length; i++) {
    iCanService.saveSensor(saveSensorDatas[i]);
  }
  res.send("OK");
});

// noinspection JSIgnoredPromiseFromCall
const portGraphql = process.env.app__server_port || 3005;
const portIoT = process.env.IoT_device_port || 1881;

graphQlServer.start({
  port: portGraphql,
  endpoint: '/graphql',
  cors: {
    origin: '*',
  }
}, (options) => {
  logger.info(options);
})
  .then(async () => {
    const appDemux = await appDemuxF();
    return appDemux.watch(false);
  })
  .then(() => {
    logger.info('---- demux was started normally');
    IoT_Device.listen(portIoT, () => {
      logger.info("IoT device listening at port ", portIoT)
    })
  })
  .catch(err => {
    logger.error(err);

  });
