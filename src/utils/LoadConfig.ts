import dotenv from 'dotenv';

/**
 * import this in front main file
 */
const loadConfigF = () => {
  // configure logging
  // console.log('NODE_ENV =', process.env.NODE_ENV);

  // load .env
  dotenv.config();

  // trace env
  // Object.keys(process.env)
  //   .filter(k => /^app__/.test(k))
  //   .forEach(k => {
  //     console.log('%s =', k, process.env[k]);
  //   });
};

loadConfigF();
