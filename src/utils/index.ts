import {BlockInfo} from 'demux';

export type ID = string | number;
export type EffectF = (state: any, payload: any, blockInfo: BlockInfo, context: any) => any;
export type UpdateF = (state: any, payload: any, blockInfo: BlockInfo, context: any) => any;
