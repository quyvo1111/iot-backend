import _ from 'lodash';
import {UINT64} from 'cuint';

export const USER_ID_GUEST = 'user-id-guest';

export const isProd = () => process.env.NODE_ENV === 'production';

// noinspection TsLint
export function isEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export const pageScrollInfoHelper = (content = [], isLast, idField = 'id') => {
  const lastItem = content[content.length - 1];
  const nextCursor = lastItem ? lastItem[idField] + 1 : undefined;

  return {content, pageScrollInfo: {nextCursor, size: content.length, isLast}};
};

export const dAppIdFromContext = (context = {}) => {
  return _.get(context, 'user.Attributes.email');
};

/**
 *
 * @param f function
 * @param t context
 * @returns {function(...[*]): Promise<any>}
 */
export const makePromise = (f, t) => (...p) =>
  new Promise((resolve, reject) => f.call(t, ...p, (err, res) => {
    if (err) {
      return reject(err);
    }

    resolve(res);
  }));

export const getUserSub = event => {
  let userSub = USER_ID_GUEST;

  if (!event || !event.requestContext) {
    return userSub;
  }

  const {requestContext} = event;

  try {
    const cognitoAuthenticationProvider = requestContext.identity.cognitoAuthenticationProvider;
    let tmp = cognitoAuthenticationProvider.split(':');
    tmp = tmp[tmp.length - 1];

    if (/^\w{8}(-\w{4}){3}-\w{12}$/.test(tmp)) {
      userSub = tmp;
    }

  } catch (err) {
    // console.error('---- getIdHelper - can not get id:', requestContext);
    // ignored this error
  }

  return userSub;
};

export function randomString(length = 7, chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
  let result = '';

  for (let i = length; i > 0; --i) {
    result += chars[Math.floor(Math.random() * chars.length)];
  }

  return result;
}

export const NOPE_FN = () => {
  // do nothing
};

export function hashString(str: string): string {
  let hash = UINT64(5381);

  for (let i = 0; i < str.length; i++) {
    hash = hash.clone().shiftl(5).add(hash).add(UINT64(str.charCodeAt(i)));
  }

  return hash.toString();
}

export function generateCanAccountName() {
  return randomString(12, '12345.abcdefghijklmnopqrstuvwxyz');
}

export function generateId() {
  return randomString(12, '12345.abcdefghijklmnopqrstuvwxyz');
}
