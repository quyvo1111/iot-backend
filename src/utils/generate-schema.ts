import pino from 'pino';
import fs from 'fs';
import path from 'path';
import {generateTypeScriptTypes} from 'graphql-schema-typescript';
import {GraphQLSchema} from 'graphql';

export function generateGraphQlTypes(schema: GraphQLSchema) {
  const logger = pino({
    name: 'setup',
    level: 'debug',
  });

  const outputPath = path.join(process.cwd(), '/src/service/GraphQlTypes.ts');
  if (!fs.existsSync(outputPath)) {
    fs.writeFileSync(outputPath, '');
  }

  generateTypeScriptTypes(schema, outputPath.toString(), {
    tabSpaces: 2,
    requireResolverTypes: false,
    typePrefix: '',
    minimizeInterfaceImplementation: true
  })
    .then(() => {
      logger.info('---- generateGraphQlTypes: DONE');
    })
    .catch(err => {
      logger.error(err);
    });
}
