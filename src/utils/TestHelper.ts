import {graphql} from '../handler';

// noinspection TsLint
const sampleEvent = (body) => ({
  "resource": "/graphql",
  "path": "/graphql",
  "httpMethod": "POST",
  "headers": {
    "Accept": "application/json, text/plain, */*",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "en-US,en;q=0.9",
    "CloudFront-Forwarded-Proto": "https",
    "CloudFront-Is-Desktop-Viewer": "true",
    "CloudFront-Is-Mobile-Viewer": "false",
    "CloudFront-Is-SmartTV-Viewer": "false",
    "CloudFront-Is-Tablet-Viewer": "false",
    "CloudFront-Viewer-Country": "VN",
    "content-type": "application/x-www-form-urlencoded",
    "Host": "vhszdxzsk8.execute-api.ap-southeast-1.amazonaws.com",
    "origin": "http://localhost:3000",
    "Referer": "http://localhost:3000/profiles",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
    "Via": "2.0 775eccfc3db436155f2d5fea77e7420a.cloudfront.net (CloudFront)",
    "X-Amz-Cf-Id": "2iEVuYkuGPPqre8Ti6sZRM3euzNXEnliIeMEtOLqEmcFxJJko9HkfQ==",
    "x-amz-date": "20180703T070322Z",
    "x-amz-security-token": "AgoGb3JpZ2luEKP//////////wEaDmFwLXNvdXRoZWFzdC0xIoACnKiiDxbTWwybxI0V4LkH1+IVXZJZICni3rFnybUkszyitIwP3YCgnneuWWC3vOTIgX/rEGdSSdYSp0WHDBBKA6Gd+m3UehBq3KpN0xbr0cz+XXj8MaVaj7BLV2lyYneBuw5p0s7ZF6WcFLgPelpNuPFSyuVIWMNCpOa2fjkLN0F6gVTKdoPN3KsvUmPEZdoTluvGhJ2VpZdc/x1vKnYUF/OqU0pSS1RBmDeDDrMDcgA7jXAgFq3MNi3rXAhLvSSNBzhEFHQQM+xEHK2KHzLqCAbKAaM5TE8PYAnDFyEWnqORJLnKXmxFbEMuyEVL4/eWcaXR+Q8i2UN5SVj4vtfn7irEBQhYEAIaDDA1OTAxNzc3ODU4NyIMFBV0lvifmx1iHnYAKqEFdIJRQMitKYfCFOGtiTX03bUpquw5aCjJypd+0VyCJcR0KAgApkSv4ZwLjYE7kImHl6q7R56kf5Wm+t6WERFF7Zy2MtrBBmdUJEs6U0tIsxSXKCcDZBZaWOHY7jpYBbKGyNNg2MkorlDar3l8B3rU/du9IbkuV9SsKBUMspWbkYl7wXeE1CG1owlIFv/ndaMtUCc8NIDBDTVllNt4I2Vu4ty4rCau70jeqKkKAu2Om4mNi2TEwT88L2FT/8XxD0Ufn85SWYGjx1rdJ8cpZD3dAp6O8Aw3Pubmc5m/2vWPTYOXg6EpzS5gzKJdwJ7MhbPE60azQmFREp4uEGgCzB+Z0RYee3QtpC1WOfEt3HcUotu4pztroPbz0jwrWg+sYjBgddtMl3pDyubkTOtka4TPJaQU0+ncm+IhSeQD5XlsZ+SXGZvad636r27MYQnMalU5xlxkookOKEAsuyNb8jZ1kbydwDmphGglac0AMm7ddQNoX0s/h4cr9m92FXcansrH2rA7dICdXNmbPWRTq0lwjtCsgMVWXUmQuBjnE8v1yLKV+uzlSs53G0gi8ra+TMonCiqSj6GdNr5sn8nGbk+WTO4m/iA1OKJfW6lfNZ1kBQ20ghobF03hEnONWuI3gMZbmknVe0H7ESdypZZZ8bvSXro0yknXYCUJx7GtmEd0ZX0w18Y5z+WM2fffhkrch+1VP6cSip2N/wWaowYQd0f2rhsQQJuX5s1988Wg1oipcyUNIjoVnEwRVzAm6YabSbjKaBYy3+7w2nj8d4e4dRFuTYRv3/VANCTHzRhJg4O/8jrzA7p5Svo5m5ZFfkV/U+asJ6P1XyKzBUFQ0xsBLsHGR8PD7ZnH6PgzzUUm2mKpwaoOIyTarolh1Zk5P0hPpiTFkDC6v+zZBQ==",
    "X-Amzn-Trace-Id": "Root=1-5b3b1fbb-6fba0d889f3e1970653f27a0",
    "X-Forwarded-For": "116.118.119.236, 54.239.129.95",
    "X-Forwarded-Port": "443",
    "X-Forwarded-Proto": "https"
  },
  "queryStringParameters": null,
  "pathParameters": null,
  "stageVariables": null,
  "requestContext": {
    "resourceId": "mqwm5c",
    "resourcePath": "/graphql",
    "httpMethod": "POST",
    "extendedRequestId": "JcHlSFc-SQ0FQXw=",
    "requestTime": "03/Jul/2018:07:03:23 +0000",
    "path": "/dev/graphql",
    "accountId": "059017778587",
    "protocol": "HTTP/1.1",
    "stage": "dev",
    "requestTimeEpoch": 1530601403480,
    "requestId": "2cf9edb5-7e8f-11e8-ba81-abb9443b8209",
    "identity": {
      "cognitoIdentityPoolId": "ap-southeast-1:e7122188-58da-4461-8bf5-3977d06469d0",
      "accountId": "059017778587",
      "cognitoIdentityId": "ap-southeast-1:c8abb1f5-e4df-48b4-b762-fc4c10945aee",
      "caller": "AROAI5W7XKM5X56ID7U26:CognitoIdentityCredentials",
      "sourceIp": "116.118.119.236",
      "accessKey": "ASIAITA5P6L7IBVTTJPQ",
      "cognitoAuthenticationType": "authenticated",
      "cognitoAuthenticationProvider": "cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_pFDR5Ab2b,cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_pFDR5Ab2b:CognitoSignIn:c041e8de-5ffc-4a2a-9e7f-3c089e5c406f",
      "userArn": "arn:aws:sts::059017778587:assumed-role/Authcan-serverdev/CognitoIdentityCredentials",
      "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36",
      "user": "AROAI5W7XKM5X56ID7U26:CognitoIdentityCredentials"
    },
    "apiId": "vhszdxzsk8"
  },
  "body": JSON.stringify(body),
  "isBase64Encoded": false
});

const parseBody = resData => {
  const body = JSON.parse(resData.body);

  const {errors, data} = body;

  if (errors) {
    // noinspection TsLint
    console.error(resData);
    throw new Error(errors[0].message);
  }

  return data;
};

export const gWrapper = (p: { query: string, variables: object }): Promise<any> => new Promise((resolve, reject) => {
  try {
    graphql(sampleEvent(p), {}, (err, res) => {
      // noinspection TsLint
      if (err) return reject(err);
      resolve(parseBody(res));
    });

  } catch (e) {
    reject(e);

  }
});
