import _ from 'lodash';
import {Updater} from 'demux';
import {USensor} from "./USensor";
import {UProfile} from "./UProfile"

const updaterList = [
  // UWhoseWallet
  UProfile,
  USensor
];

export function updaterF(account: string) {
  const res = updaterList
    .map(U => new U())
    .map(u => Object.keys(u).map(
      (k): Updater => ({
        actionType: `${account}::${k}`,
        updater: u[k]
      })
    ));

  return _.flatten(res);
}
