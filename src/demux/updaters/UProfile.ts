import {UpdateF} from "../../utils";
import {BlockInfo} from "demux";
import {iProfile} from "../../service";
import {logger} from "../../utils/LogConfig";

export class UProfile {
  createprl: UpdateF = async (state: any, payload: any, blockInfo: BlockInfo, context: any) => {
    logger.debug('---- create new profile, payload is:', payload);
    const data = payload.data;
    await iProfile.createProfile(payload.data);
  };
  
}
