import {UpdateF} from "../../utils";
import {BlockInfo} from "demux";
import {iSensor, iSensorData} from "../../service";
import {logger} from "../../utils/LogConfig";

export class USensor {

  createsensor: UpdateF = async (state: any, payload: any, blockInfo: BlockInfo, context: any) => {
    logger.debug('---- create new sensor, payload is:', payload);
    const data = payload.data;
    await iSensor.createSensor(data);
  };

  save: UpdateF = async (state: any, payload: any, blockInfo: BlockInfo, context: any) => {
    logger.debug('---- new sensor data, payload is:', payload);
    const data = payload.data;
    await iSensorData.saveSensorData(data);
  };

  del: UpdateF = async (state: any, payload: any, blockInfo: BlockInfo, context: any) => {
    logger.debug('---- delete sensor data, payload is:', payload);
    const data = payload.data;
    await iSensorData.delete(data);
  };
}
