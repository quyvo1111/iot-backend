import {BaseActionWatcher} from 'demux';
import {NodeosActionReader} from 'demux-eos';
import {ActionHandler} from './ActionHandler';
import {effectF} from './effects';
import {updaterF} from './updaters';
import {logger} from "../utils/LogConfig";
import {iBlock} from "../service";

export const appDemuxF = async (): Promise<BaseActionWatcher> => {
  const eosAccount: string = process.env.app__eos_account;
  logger.debug('---- Contract owner:', eosAccount);

  const effects = effectF(eosAccount);
  const updaters = updaterF(eosAccount);

  const indexState = await iBlock.loadIndexState();

  return new BaseActionWatcher(
    new NodeosActionReader(
      process.env.app__eosio_http_url,
      indexState.blockNumber // First actions relevant to this dApp happen at this block
    ),
    new ActionHandler(updaters, effects),
    1000 // Poll at twice the block interval for less latency,
  );
};
