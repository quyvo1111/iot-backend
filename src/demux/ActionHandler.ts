import {AbstractActionHandler, Block, Effect, IndexState, Updater} from 'demux';
import {logger} from '../utils/LogConfig';
import {iBlock} from '../service';

// Initial state
const state = {
  indexState: {
    blockNumber: 0,
    blockHash: ""
  }
};

export class ActionHandler extends AbstractActionHandler {
  constructor(updaters: Updater[], effects: Effect[]) {
    super(updaters, effects);
  }

  protected async handleWithState(handle: (state: any, context?: any) => void): Promise<void> {
    const context = {};
    try {
      return await handle(state, context);
    } catch (err) {
      logger.error(err);
    }
  }

  protected loadIndexState(): Promise<IndexState> {
    return iBlock.loadIndexState();
  }

  protected async rollbackTo(blockNumber: number): Promise<void> {
    return logger.debug('---- roll back to:', blockNumber);
  }

  // noinspection TsLint
  protected updateIndexState(state: any, block: Block, isReplay: boolean, context?: any): Promise<void> {
    return iBlock.updateIndexState(state, block, isReplay, context);
  }
}
