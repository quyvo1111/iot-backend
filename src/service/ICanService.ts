export interface ICanService {
  createCanAccount(canAccount: string, ownerKey: string, activeKey: string): Promise<Boolean>;
  createProfile(createProfileInput: any): Promise<Boolean>;
  createSensor(createSensorInput: any): Promise<Boolean>;
  saveSensor(saveSensorInput: any): Promise<Boolean>;
}