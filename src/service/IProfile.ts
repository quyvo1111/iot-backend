import {IAbsService} from './IAbsService';

export interface IProfile extends IAbsService {
  createProfile(profile): Promise<any>;

  updateProfile(id, profile, context): Promise<any>;

  getProfile(id, context): Promise<any>;

  updateCanAccounts(id, canAccounts, context): Promise<any>;

}
