import {DynamoDB} from "aws-sdk";

export interface IDynamoDb {
  getDb(): DynamoDB;

  getDDb(): DynamoDB.DocumentClient;

  getTableName(): string;

  put(Item): Promise<any>;

  delete(pk: string, id: string): Promise<any>;

  getOne(pk: string, id: string): Promise<any>;
}
