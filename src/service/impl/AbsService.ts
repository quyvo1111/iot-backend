import _ from 'lodash';
import {iDynamoDb} from "../index";
import {IAbsService} from "../IAbsService";
import {logger} from '../../utils/LogConfig';
import {ID} from '../../utils';

export abstract class AbsService implements IAbsService {

  protected constructor(protected entityName: string) {
  }

  async findById(id: string): Promise<any> {
    const res = await iDynamoDb.getOne(this.entityName, id);
    if (!res.Item) {
      throw new Error('Item was not found: ' + id);
    }

    return res.Item;
  }

  async save(Item): Promise<any> {
    return this._save(Item, false);
  }

  async _save(Item, isUpdate): Promise<any> {
    Item.pk = this.entityName;

    if (_.isEmpty(Item.id)) {
      Item.id = await this.nextId();
      Item.id = String(Item.id);
    }

    try {
      await iDynamoDb.getDDb().put({
        Item,
        TableName: iDynamoDb.getTableName(),
        ConditionExpression: isUpdate ? undefined : "attribute_not_exists(id)"
      }).promise();

      return Item;

    } catch (err) {
      logger.debug(err);
      throw new Error('Item may exist');

    }
  }

  async delete(id: ID): Promise<any> {
    const strId: string = id + '';
    return iDynamoDb.delete(this.entityName, strId)
  }

  /**
   * TODO fix this update function
   * @param id
   * @param item
   */
  update(id: ID, item): Promise<any> {
    item.id = id;
    return this._save(item, true);
  }

  async nextId(): Promise<ID> {
    try {
      const res = await iDynamoDb.getDDb().update({
        Key: {
          pk: 'dynamodb_sequence',
          id: this.entityName
        },
        UpdateExpression: 'SET seq = seq + :value',
        ExpressionAttributeValues: {':value': 1},
        TableName: iDynamoDb.getTableName(),
        ReturnValues: "ALL_NEW",
      }).promise();

      return res.Attributes.seq;

    } catch (err) {
      if (err.message === 'The provided expression refers to an attribute that does not exist in the item') {
        await iDynamoDb.put({
          pk: 'dynamodb_sequence',
          id: this.entityName,
          seq: 0
        });

        return await this.nextId();
      }

      throw err;
    }
  }
}
