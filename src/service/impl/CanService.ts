import {ICanService} from "../ICanService";
import {logger} from "../../utils/LogConfig";
import moment from 'moment';

export class CanService implements ICanService {

  async createCanAccount(canAccount: string, ownerKey: string, activeKey: string): Promise<Boolean> {

    const eosjs = require('eosjs');
    const fetch = require('node-fetch');
    const {TextDecoder, TextEncoder} = require('text-encoding');
    const defaultPrivateKey = '5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3';
    const signatureProvider = new eosjs.SignatureProvider([defaultPrivateKey]);
    const rpc = new eosjs.Rpc.JsonRpc(process.env['app__eosio_http_url'], {fetch});
    const api = new eosjs.Api({
      rpc, signatureProvider,
      textDecoder: new TextDecoder,
      textEncoder: new TextEncoder
    });
    console.log(canAccount);

    try {
      const result = await api.transact({
        actions: [{
          account: 'eosio',
          name: 'newaccount',
          authorization: [{
            actor: 'iotsc',
            permission: 'active',
          }],
          data: {
            creator: 'iotsc',
            name: canAccount,
            owner: {
              threshold: 1,
              keys: [{
                key: ownerKey,
                weight: 1
              }],
              accounts: [],
              waits: []
            },
            active: {
              threshold: 1,
              keys: [{
                key: activeKey,
                weight: 1
              }],
              accounts: [],
              waits: []
            },
          },
        },
          {
            account: 'eosio',
            name: 'buyrambytes',
            authorization: [{
              actor: 'iotsc',
              permission: 'active',
            }],
            data: {
              payer: 'iotsc',
              receiver: canAccount,
              bytes: 8192,
            },
          },
          {
            account: 'eosio',
            name: 'delegatebw',
            authorization: [{
              actor: 'iotsc',
              permission: 'active',
            }],
            data: {
              from: 'iotsc',
              receiver: canAccount,
              stake_net_quantity: '1.0000 SYS',
              stake_cpu_quantity: '1.0000 SYS',
              transfer: false,
            }
          }]
      }, {
        blocksBehind: 3,
        expireSeconds: 30,
      });
    } catch (e) {
      logger.debug('---- Caught exception: ', JSON.stringify(e.json, null, 2));
      return false;
    }
    return true;
  }

  async createProfile(createProfileInput): Promise<Boolean> {

    const eosjs = require('eosjs');
    const fetch = require('node-fetch');
    const {TextDecoder, TextEncoder} = require('text-encoding');
    const defaultPrivateKey = createProfileInput.private_key;
    const signatureProvider = new eosjs.SignatureProvider([defaultPrivateKey]);
    const rpc = new eosjs.Rpc.JsonRpc(process.env['app__eosio_http_url'], {fetch});
    const api = new eosjs.Api({
      rpc, signatureProvider,
      textDecoder: new TextDecoder,
      textEncoder: new TextEncoder
    });
    console.log(createProfileInput);

    try {
      const result = await api.transact({
        actions: [{
          account: 'iotsc',
          name: 'createprl',
          authorization: [{
            actor: createProfileInput.eos_account,
            permission: 'active',
          }],
          data: {
            owner: createProfileInput.eos_account,
            name: createProfileInput.name,
            company: createProfileInput.company,
            telephone: createProfileInput.telephone,
            email: createProfileInput.email,
            address: createProfileInput.address,
            description: createProfileInput.description
          }
        }
        ]
      }, {
        blocksBehind: 3,
        expireSeconds: 30,
      });
    } catch (e) {
      logger.debug('---- Caught exception: ', JSON.stringify(e.json, null, 2));
      return false;
    }
    return true;
  }

  async createSensor(createSensorInput): Promise<Boolean> {

    const eosjs = require('eosjs');
    const fetch = require('node-fetch');
    const {TextDecoder, TextEncoder} = require('text-encoding');
    const defaultPrivateKey = createSensorInput.private_key;
    const signatureProvider = new eosjs.SignatureProvider([defaultPrivateKey]);
    const rpc = new eosjs.Rpc.JsonRpc(process.env['app__eosio_http_url'], {fetch});
    const api = new eosjs.Api({
      rpc, signatureProvider,
      textDecoder: new TextDecoder,
      textEncoder: new TextEncoder
    });
    console.log(createSensorInput);
    console.log(moment().format('YYYY-MM-DDTh:mm:ss'));

    try {
      const result = await api.transact({
        actions: [{
          account: 'iotsc',
          name: 'createsensor',
          authorization: [{
            actor: createSensorInput.eos_account,
            permission: 'active',
          }],
          data: {
            owner: createSensorInput.eos_account,
            type: createSensorInput.type,
            address: createSensorInput.address,
            description: createSensorInput.description,
            start_time: moment().format('YYYY-MM-DDTh:mm:ss') + '',
          }
        }
        ]
      }, {
        blocksBehind: 3,
        expireSeconds: 30,
      });
    } catch (e) {
      logger.debug('---- Caught exception: ', JSON.stringify(e.json, null, 2));
      return false;
    }
    return true;
  }

  async saveSensor(saveSensorInput): Promise<Boolean> {

    const eosjs = require('eosjs');
    const fetch = require('node-fetch');
    const {TextDecoder, TextEncoder} = require('text-encoding');
    const defaultPrivateKey = saveSensorInput.private_key;
    const signatureProvider = new eosjs.SignatureProvider([defaultPrivateKey]);
    const rpc = new eosjs.Rpc.JsonRpc(process.env['app__eosio_http_url'], {fetch});
    const api = new eosjs.Api({
      rpc, signatureProvider,
      textDecoder: new TextDecoder,
      textEncoder: new TextEncoder
    });
    console.log(saveSensorInput);
    console.log(moment().format('YYYY-MM-DDTh:mm:ss'));

    try {
      const result = await api.transact({
        actions: [{
          account: 'iotsc',
          name: 'save',
          authorization: [{
            actor: saveSensorInput.eos_account,
            permission: 'active',
          }],
          data: {
            owner: saveSensorInput.eos_account,
            type: saveSensorInput.type,
            value: saveSensorInput.value,
            timestamp: moment().format('YYYY-MM-DDTHH:mm:ss') + '',
          }
        }
        ]
      }, {
        blocksBehind: 3,
        expireSeconds: 30,
      });
    } catch (e) {
      logger.debug('---- Caught exception: ', JSON.stringify(e.json, null, 2));
      console.log('---- error when udpate data to Blockchain: ', e.toString());
      return false;
    }
    return true;
  }

}
