import {IProfile} from '../IProfile';
import {AbsService} from './AbsService';
import {Profile} from '../GraphQlTypes';
import {iDynamoDb} from "../index";

export class ProfileService extends AbsService implements IProfile {
  constructor() {
    super('Profile');
  }

  createProfile(profile): Promise<any> {
    profile.id = profile.owner;
    return this.save(profile);
  }

  getProfile(id, context): Promise<any> {
    return this.findById(id);
  }

  updateProfile(id, profile, context): Promise<any> {
    return this.update(id, profile);
  }

  async updateCanAccounts(id, canAccounts, context) {
    const ProfileInfo = await this.findById(id);
    const addCanAccounts = canAccounts.concat(ProfileInfo.canAccounts);

    return iDynamoDb.getDDb().update({
      UpdateExpression: 'SET canAccounts= :account',
      ExpressionAttributeValues: {
        ":account": addCanAccounts
      },
      TableName: iDynamoDb.getTableName(),
      Key: {
        pk: this.entityName,
        id
      }
    }).promise();
  }

}
