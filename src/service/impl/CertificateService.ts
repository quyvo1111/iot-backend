import {AbsService} from './AbsService';
import {ICertificate} from '../ICertificate';

export class CertificateService extends AbsService implements ICertificate {
  constructor() {
    super('Certificate');
  }

  createCertificate(certificate, context): Promise<any> {
    return this.save(certificate);
  }

  getCertificate(id, context): Promise<any> {
    return undefined;
  }

  issuedCertificates(pageScrollInput, context): Promise<any> {
    return undefined;
  }

  myCertificates(pageScrollInput, context): Promise<any> {
    return undefined;
  }

  revokeCertificate(revokeCertificate, context): Promise<any> {
    return undefined;
  }
}
