import {ISensorData} from '../ISensorData';
import {AbsService} from './AbsService';
import {iDynamoDb} from "../index";
import {logger} from '../../utils/LogConfig';
import moment from 'moment';

export class SensorDataService extends AbsService implements ISensorData {
  constructor() {
    super('SData');
  }

  async saveSensorData(data){
    data.pk = data.owner + data.type;

    data.id = data.timestamp;

    const Item = data;

    try {
      await iDynamoDb.getDDb().put({
        Item,
        TableName: iDynamoDb.getTableName(),
        ConditionExpression: "attribute_not_exists(id)"
      }).promise();

      return data;

    } catch (err) {
      logger.debug(err);
      throw new Error('Item may exist');

    }
  }

  async getRecentData(size, owner, type){
    // todo get lastest data, and calculate start point datatime for the next query
    const params = {
      TableName: iDynamoDb.getTableName(),
      KeyConditionExpression: 'pk = :pk',
      FilterExpression: "#type = :type",
      ExpressionAttributeNames: {
        '#type': 'type'
      },
      ExpressionAttributeValues: {
        ':type': type,
        ':pk': owner + type
      },
      ScanIndexForward: false,
      Limit: 1
    };

    let res = await iDynamoDb.getDDb().query(params).promise();

    const paramsQuery = {
      TableName: iDynamoDb.getTableName(),
      KeyConditionExpression: 'pk = :pk',
      FilterExpression: "#type = :type",
      ExpressionAttributeNames: {
        '#type': 'type'
      },
      ExpressionAttributeValues: {
        ':type': type,
        ':pk': owner + type
      },
      ExclusiveStartKey: {
        id: moment(res.Items[0].id).subtract(size, 'hours').format('YYYY-MM-DDTHH:mm:ss'),
        pk: owner + type
      }
    };

    res = await iDynamoDb.getDDb().query(paramsQuery).promise();

    return res.Items;

  }

  async getSensorData(from, size, owner, type, context){
    const params = {
      TableName: iDynamoDb.getTableName(),
      KeyConditionExpression: 'pk = :pk',
      FilterExpression: "#type = :type",
      ExpressionAttributeNames: {
        '#type': 'type'
      },
      ExpressionAttributeValues: {
        ':type': type,
        ':pk': owner + type
      },
      ExclusiveStartKey: {pk: owner, id: from},
      Limit: size
    };

    const res = await iDynamoDb.getDDb().query(params).promise();

    return res.Items;

  }

  async deleteSensorData(owner, id){
    const strId: string = id + '';
    return iDynamoDb.delete(owner, strId)
  }

}
