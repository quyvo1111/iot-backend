import {Block, IndexState} from 'demux';
import {AbsService} from './AbsService';
import {IBlock} from '../IBlock';

export class BlockService extends AbsService implements IBlock {
  indexStateId: string = 'index_state';

  constructor() {
    super('Block');
  }

  async loadIndexState(): Promise<IndexState> {
    try {
      return await this.findById(this.indexStateId);

    } catch (e) {

      return {
        blockNumber: parseInt(process.env.app__eosio_starting_block, 10) || 0,
        blockHash: ''
      };
    }
  }

  updateIndexState(state: any, block: Block, isReplay: boolean, context?: any): Promise<void> {
    const {blockInfo: {blockNumber, blockHash}} = block;
    const indexState: IndexState = {
      blockNumber,
      blockHash
    };

    return this.update(this.indexStateId, indexState);
  }

}
