import {AbsService} from './AbsService';
import {ISensor} from "../ISensor";
import {iDynamoDb} from "../index";
import {generateId} from "../../utils/CommonUtils";

export class SensorService extends AbsService implements ISensor {
  constructor() {
    super('Sensor');
  }

  async createSensor(sensorDetail){
    sensorDetail.id = generateId();
    sensorDetail.pk = this.entityName;
    return this.save(sensorDetail);

    try {
      await iDynamoDb.getDDb().put({
        Item: sensorDetail,
        TableName: iDynamoDb.getTableName(),
        ConditionExpression: "attribute_not_exists(id)"
      }).promise();

      return sensorDetail;

    } catch (err) {
      return null;

    }
  }

  async getSensorDetail(id, context){
    const res = await iDynamoDb.getOne(this.entityName, id);
    if (!res.Item) {
      return null
    }

    return res.Item;
  }

  async getSensorDetailByOwner(owner, context){
    const res = await iDynamoDb.getDDb().query({
      TableName: iDynamoDb.getTableName(),
      KeyConditionExpression: "pk = :pk",
      FilterExpression: '#owner = :owner',
      ExpressionAttributeNames: {
        '#owner' : 'owner'
      },
      ExpressionAttributeValues: {
        ':pk': this.entityName,
        ':owner': owner
      },
    }).promise();

    return res.Items;
  }
}
