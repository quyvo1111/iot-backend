import {DynamoDB} from 'aws-sdk';
import {IDynamoDb} from "../IDynamoDb";

export class DynamoDbService implements IDynamoDb {
  private readonly db: DynamoDB;
  private readonly ddb: DynamoDB.DocumentClient;

  constructor() {
    const options = {
      apiVersion: '2012-08-10',
      region: process.env.app__aws_region || 'ap-southeast-1',
      endpoint: process.env.app__DynamoDb_endpoint,
      accessKeyId: process.env.app__access_key_id,
      secretAccessKey: process.env.app__access_key_secret,
    };

    this.db = new DynamoDB(options);
    this.ddb = new DynamoDB.DocumentClient(options);
  }

  getTableName(): string {
    if (process.env.NODE_ENV === 'development') {
      return 'can_pass_dev';
    }

    return process.env.app__DYNAMODB_TABLE || 'can_pass_dev';
  }

  getDDb(): DynamoDB.DocumentClient {
    return this.ddb;
  }

  getDb(): DynamoDB {
    return this.db;
  }

  put(Item): Promise<any> {
    return this.ddb.put({
      Item,
      TableName: this.getTableName(),
    }).promise();
  }

  delete(pk: string, id: string): Promise<any> {
    return this.ddb.delete({
      Key: {pk, id},
      TableName: this.getTableName(),
    }).promise();
  }

  getOne(pk: string, id: string): Promise<any> {
    return this.ddb.get({
      Key: {pk, id},
      TableName: this.getTableName()
    }).promise();
  }
}
