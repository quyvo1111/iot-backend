import {IAbsService} from './IAbsService';

export interface ISensor extends IAbsService {
   createSensor(SensorInfo): Promise<any>

   getSensorDetailByOwner(owner, context): Promise<any>

   getSensorDetail(id, context): Promise<any>
}
