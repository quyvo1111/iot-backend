import {IAbsService} from './IAbsService';

export interface ISensorData extends IAbsService {
    saveSensorData(data): Promise<any>
    getSensorData(from, to, owner, type, context): Promise<any>
    getRecentData(size, owner, type): Promise<any>
    deleteSensorData(owner, id): Promise<any>
}
