import CognitoExpress from 'cognito-express';
import {logger} from '../utils/LogConfig';

export function validateToken(token: string, type: 'id' | 'access'): Promise<any> {
  return new Promise((resolve, reject) => {
    const cognitoExpress = new CognitoExpress({
      region: process.env.app__aws_region,
      cognitoUserPoolId: process.env.app__cognito_userPoolId,
      tokenUse: type, // Possible Values: access | id
      tokenExpiration: 3600000 // Up to default expiration of 1 hour (3600000 ms)
    });

    cognitoExpress.validate(token, (err, res) => {
      if (err) return reject(err);
      resolve(res);
    });
  });
}

export async function authMdw(resolve, parent, args, ctx, info) {
  logger.debug('---- args:', args);
  logger.debug('---- context:', ctx);

  return await resolve(parent, args, ctx, info);
}
