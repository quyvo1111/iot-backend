import {ID} from '../utils';

export interface IAbsService {
  nextId(): Promise<ID>;

  save(item): Promise<any>;

  delete(id: ID): Promise<any>;

  update(id: ID, item): Promise<any>;

  findById(id: ID): Promise<any>;
}
