import {Block, IndexState} from 'demux';

export interface IBlock {
  loadIndexState(): Promise<IndexState>;

  updateIndexState(state: any, block: Block, isReplay: boolean, context?: any): Promise<void>;
}
