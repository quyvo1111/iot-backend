import {iSensor} from "../src/service";
import {gWrapper} from "../src/utils/TestHelper";

describe('test Sensor Detail service', () => {

  let nSensor;

  afterEach(async () => {

    // clean
    if (nSensor) {
      // await iSensor.delete(nSensor.id);
      nSensor = undefined;
    }

  });

  const SensorInput = {
    owner: "quyvo",
    type: "temp",
    company: "university of sciences",
    address: "thua thien hue",
    description: "iot sensor data",
    start_time: "2017:09:20T19:11:11"
  };

  it('should get sensor detail', async () => {
    nSensor = await iSensor.createSensor(SensorInput);

    const res = await gWrapper({
      query: `query sensorDetail($id: ID!) {
        sensorDetail(id: $id){
            owner
        }
      }`,

      variables: {
        id: nSensor.id
      }
    });
    console.log(res);
    expect(res.sensorDetail.owner).toBe(SensorInput.owner);
  });

  it('should get sensor detail by owner', async () => {
    nSensor = await iSensor.createSensor(SensorInput);

    const res = await gWrapper({
      query: `query getSensorDetailByOwner($owner: String!) {
        getSensorDetailByOwner(owner: $owner){
            owner
        }
      }`,

      variables: {
        owner: nSensor.owner
      }
    });

    expect(res.getSensorDetailByOwner).toEqual(expect.arrayContaining([{owner: "quyvo"}]));
  });

});
