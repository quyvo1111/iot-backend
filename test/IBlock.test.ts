import {iBlock} from "../src/service";

describe('test IBlock.test', () => {
  it('should find recent block', async () => {
    const ids = await iBlock.loadIndexState();
    expect(ids.blockHash).not.toEqual(null);
    expect(ids.blockNumber).not.toEqual(null);
  });
});
