import {dAppIdFromContext, generateCanAccountName, hashString} from "../src/utils/CommonUtils";

describe('test common utils', () => {
  const context = {
    user: {
      Username: 'CBadge',
      Attributes: {
        email: 'manhvv2.100@yopmail.com'
      }
    }
  };

  it('get dapp id from context', () => {
    const dAppId = dAppIdFromContext(context);
    expect(dAppId).toBe(context.user.Attributes.email);
  });

  it('should generate hash number of a 1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX', async () => {
    const myString = '1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX';
    const hash = hashString(myString);
    expect(hash).toBe('5928833083981266563');
  });

  it('should generate a name to be exactly 12 chars with specific domain chars [a-z.][1-5]', () => {
    const canAccount = generateCanAccountName();
    expect(canAccount.length).toBe(12);
    expect(/[a-z.1-5]/.test(canAccount)).toBe(true);
  });
});
