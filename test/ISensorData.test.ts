import {gWrapper} from '../src/utils/TestHelper';
import {iSensorData} from "../src/service";

describe('ISensorData', () => {
  let nSensorData = [];

  afterEach(async () => {

    // clean
    for (const data of nSensorData){
      // await iSensorData.deleteSensorData(dataInput.owner, data.id);
      continue
    }
    nSensorData = [];
   });

  const dataInput = {
        owner: "quyvo",
        type: "temp"
  };

  it('should get sensor data', async () => {
    for (let i = 0; i < 10; i++) {
        nSensorData.push(await iSensorData.saveSensorData({
          ...dataInput,
          timestamp: "2018-09-09T19:09:0" + i,
          value: Math.floor(Math.random() * 10)
        }));
    }

    const res = await gWrapper({
      query: `query SensorData($from: String!, $size: Int!, $owner: String!, $type: String!) {
        SensorData(from: $from,size: $size, owner: $owner, type: $type){
            timestamp
        }
      }`,

      variables: {
        from: "2018-09-09T19:09:00",
        size: 10,
        owner: "quyvo",
        type: "temp"
      }
    });

    expect(res.SensorData).toEqual(expect.arrayContaining([{ timestamp: '2018-09-09T19:09:01' }]));
  });

  it('should get recent sensor data', async () => {
    for (let i = 0; i < 10; i++) {
      nSensorData.push(await iSensorData.saveSensorData({
        ...dataInput,
        timestamp: "2018-09-09T19:09:0" + i,
        value: Math.floor(Math.random() * 10)
      }));
    }

    const res = await gWrapper({
      query: `query RecentData($size: Int!, $owner: String!, $type: String!) {
        RecentData(size: $size, owner: $owner, type: $type){
            timestamp
        }
      }`,

      variables: {
        size: 5,
        owner: "quyvo",
        type: "temp"
      }
    });

    expect(res.RecentData).toEqual(expect.arrayContaining([{ timestamp: '2018-09-09T19:09:07' }]));
    expect(res.RecentData).not.toEqual(expect.arrayContaining([{ timestamp: '2018-09-09T19:09:01' }]));
  });
  
});
