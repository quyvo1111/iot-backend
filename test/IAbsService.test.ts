import {iAbsService} from "../src/service";

describe('test abstract service', () => {
  it('should return new item after save', async () => {
    const item = {
      name: 'test return new item',
    };

    const nItem = await iAbsService.save(item);
    expect(nItem).toBeTruthy();
    expect(nItem.id).toBeTruthy();
    expect(nItem.name).toBe(item.name);

    // clean
    await iAbsService.delete(nItem.id);
  });

  it('should delete item', async () => {
    // create item
    const nItem = await iAbsService.save({
      name: 'this item is for test'
    });

    expect(nItem).toBeTruthy();

    // test delete function
    await iAbsService.delete(nItem.id);
    expect(true).toBe(true);
  });

  it('should through exception if save the item with same id', async () => {
    const item = {
      id: 'same-id',
      name: 'this is item'
    };

    // save 1
    const s1 = await iAbsService.save(item);
    expect(s1).toBeTruthy();

    // save 2
    const p = iAbsService.save(item);
    await expect(p).rejects.toThrowError('Item may exist');

    // clean
    await iAbsService.delete(s1.id);
  });

  it('should find item by id', async () => {
    const item = {
      id: 'item-id',
      name: 'this is for finding'
    };

    // save 1
    const s1 = await iAbsService.save(item);
    expect(true).toBe(true);

    const f = await iAbsService.findById(s1.id);
    expect(f.id).toBe(s1.id);

    // clean
    await iAbsService.delete(s1.id);
  });

});
