import {iDynamoDb} from "../src/service/index";

describe('DynamoDbService.ts', async () => {
  const testTbl = 'testTbl';

  it('should success connect to DB', async () => {
    const req = iDynamoDb.getDDb().put({
      Item: {
        pk: testTbl,
        id: 'test-item',
        checkHealth: true
      },
      TableName: iDynamoDb.getTableName(),
    });

    const res = await req.promise();
    expect(res).toBeTruthy();
  });
});