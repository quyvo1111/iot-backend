import {validateToken} from '../src/service/Authentication';
import {logger} from '../src/utils/LogConfig';

describe.skip('Authentication', () => {
  it('should verify jwt idToken from cognito', async () => {
    const idToken = 'eyJraWQiOiJVU2tzU3NpQzA1bE9DOXlRSXhMSDBxcWEzeWo0aWx3NUF0aXZFOEdFVEdFPSIsImFsZyI6IlJTM' +
      'jU2In0.eyJhdF9oYXNoIjoiLUQydHVrN202UlNoVnh6RmdBd0ZaUSIsInN1YiI6Ijc0ZGEwZmQwLWU5ZDctNDg4NC1hMjFkLTYy' +
      'MzBlOWI1Y2QzYiIsImF1ZCI6IjFtbjY3c3NkcGQxb2tja2lhazFrazVpaWFhIiwiZXZlbnRfaWQiOiIwNWY1ZDM5ZS1iZDUwLTE' +
      'xZTgtYTBhYi03N2EwNzU5YmQxZTQiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTUzNzUwMTIwMywiaXNzIjoiaHR0cH' +
      'M6XC9cL2NvZ25pdG8taWRwLmFwLXNvdXRoZWFzdC0xLmFtYXpvbmF3cy5jb21cL2FwLXNvdXRoZWFzdC0xX1ZyMDlaRXJWSyIsI' +
      'mNvZ25pdG86dXNlcm5hbWUiOiJtYW5odnUxIiwiZXhwIjoxNTM3NTA0ODAzLCJpYXQiOjE1Mzc1MDEyMDN9.VWJPyv_0no6XBge' +
      'WDq9JEoLIetkmKclwXVXOFIXrwaVo69xUpHntXg4lyjfrDLOS03JBm22Mc_FaPzNWC9-dNXacnDqJdsqkx8fhZP_Lbmz8LC97U3' +
      'E1oUARX8Bs9W1zC4wVyFZvJnqJ407sveB9erUekJFaUobVL5zk6BEM9YWHqo_NchQ4Jh5u7IAoIFWENZBj7VXoHrQr4Xrp7p0VC' +
      '0-NeiwzlIYVZDr5yBB1Ww-q9yivOtxJPpe_BG64yWgoXCwQ8Lf14fKFt1Yp_vqeNGokuI_Em7lOz3Xxe5jLAsmpqvomRi8L9cz2' +
      'xW9vt37KCJVky4E30sHW8j7qRZH9jQ';

    const res = await validateToken(idToken, 'id');

    logger.debug(res);

    expect(res.sub).toBe('74da0fd0-e9d7-4884-a21d-6230e9b5cd3b');
  });

  it('should verify jwt accessToken from cognito', async () => {
    const accessToken = 'eyJraWQiOiJlK29UaXVNSVhJanJ1NlMyN0xSY2V0ckFYNUxxRlV4WDFGTDJFbk9YMzR3PSIsImFsZyI6Il' +
      'JTMjU2In0.eyJzdWIiOiI3NGRhMGZkMC1lOWQ3LTQ4ODQtYTIxZC02MjMwZTliNWNkM2IiLCJldmVudF9pZCI6IjA1ZjVkMzllLWJ' +
      'kNTAtMTFlOC1hMGFiLTc3YTA3NTliZDFlNCIsInRva2VuX3VzZSI6ImFjY2VzcyIsInNjb3BlIjoiaHR0cHM6XC9cLzExNi4xMTgu' +
      'MTE5LjIzNlwvbXZ2LXJlc291cmNlLXNlcnZlclwvcmVhZCBvcGVuaWQgZW1haWwgaHR0cDpcL1wvMTE2LjExOC4xMTkuMjM2OjMwMDZ' +
      'cL2VtYWlsLnJlYWQiLCJhdXRoX3RpbWUiOjE1Mzc1MDEyMDMsImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5hcC1zb3V0aGVhc3' +
      'QtMS5hbWF6b25hd3MuY29tXC9hcC1zb3V0aGVhc3QtMV9WcjA5WkVyVksiLCJleHAiOjE1Mzc1MDQ4MDMsImlhdCI6MTUzNzUwMTIwM' +
      'ywidmVyc2lvbiI6MiwianRpIjoiMmZiYzI1NzUtN2NkNC00Zjk5LTk5MzMtNmExYmFmYjc3Yzg1IiwiY2xpZW50X2lkIjoiMW1uNjdz' +
      'c2RwZDFva2NraWFrMWtrNWlpYWEiLCJ1c2VybmFtZSI6Im1hbmh2dTEifQ.Fyr3QcQstfbFKA2l57X5XryWRNOo4IazDF_DGts5zgrB3' +
      'cHDWA0f3zWLwiHCqT1_TKGroqSSWF9NUlsvWUyLK2ltB1-t4JXhPAAc4Q49vjTn4udueMS2XJcNrehSf63AG9IGwkzlOYXvIPWxoo31n' +
      'ptJVcx6KCfWFWtqxXWVuJy2Ep2gVzqbm2Dr2cCaSq_a4VBy_TAAZckmkx8XAm1bwgzFJOJp1qhpz3ltau0HfI__675d68wTftBxKhN0sV' +
      '1ORUnIXu9XVlowl3GAOjtu110tW18MdpejNIw-AKNiuIFXp5L2m9YBKki8z7u_6gPp75Hx51OwgV5B_dq5LlOFcg';

    const res = await validateToken(accessToken, 'access');

    logger.debug(res);

    expect(res.sub).toBe('74da0fd0-e9d7-4884-a21d-6230e9b5cd3b');
  });
});
