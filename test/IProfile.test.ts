import {gWrapper} from '../src/utils/TestHelper';
import {iProfile} from "../src/service";

describe('IProfile', () => {
  let nProfile;

  afterEach(async () => {

    // clean
    if (nProfile) {
      //await iProfile.delete(nProfile.id);
      nProfile = undefined;
    }

  });

  const profileInput = {
        owner: "quyvo",
        name: "quyvo",
        company: "university of sciences",
        telephone: "0975449712",
        email: "quyvok37@gmail.com",
        address: "thua thien hue",
        description: "iot sensor data" 
  };

  it('should get profile', async () => {
    nProfile = await iProfile.createProfile(profileInput);

    const res = await gWrapper({
      query: `query profile($id: ID!) {
        profile(id: $id){
            owner
            name
        }
      }`,

      variables: {
        id: nProfile.id
      }
    });

    expect(res.profile.owner).toBe(profileInput.owner);
  });
  
});
