import {gWrapper} from "../src/utils/TestHelper";
import {generateCanAccountName} from "../src/utils/CommonUtils";
import {iCanService} from "../src/service";

describe.skip('test can service', () => {

  it('test create a new CAN account', async () => {
    const canAccount = generateCanAccountName();
    const ownerKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';
    const activeKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';

    const create = await iCanService.createCanAccount(canAccount, ownerKey, activeKey);

    expect(create).toBe(true);
  });

  it('test create a new profile', async () => {
    const profileInput = {
      private_key: '5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3',
      eos_account: 'daniel',
      owner: 'daniel',
      name: 'daniel',
      company: 'usc',
      telephone: '0934',
      email: 'daniel@gmail.com',
      address: 'ho chi minh',
      description: 'test from server'
    }

    const create = await iCanService.createProfile(profileInput);

    expect(create).toBe(true);
  });

  it('test create a new sensor detail', async () => {
    const sensorInput = {
      private_key: '5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3',
      eos_account: 'daniel',
      owner: 'daniel',
      type: 'temp',
      address: 'ho chi minh',
      description: 'test from server'
    }

    const create = await iCanService.createSensor(sensorInput);

    expect(create).toBe(true);
  });

  fit('test save sensor data', async () => {
    const sensorInput = {
      private_key: '5KQwrPbwdL6PhXujxW37FSSQZ1JiwsST4cqQzDeyXtP79zkvFD3',
      eos_account: 'quyvo',
      type: 'temperature',
      value: 31
    }

    const create = await iCanService.saveSensor(sensorInput);

    expect(create).toBe(true);
  });

  it('creating a duplicate CAN account should catch exception then return false', async () => {
    jest.setTimeout(10000);

    const canAccount = generateCanAccountName();
    const ownerKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';
    const activeKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';

    const create = await iCanService.createCanAccount(canAccount, ownerKey, activeKey);
    expect(create).toBe(true);

    const re_create = await iCanService.createCanAccount(canAccount, ownerKey, activeKey);
    expect(re_create).toBe(false);
  });

  /**
   * This test case was ignored due to relate to eos node
   */
  it('querry GraphQl should create a new CAN account', async () => {
    const canAccount = generateCanAccountName();
    const ownerKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';
    const activeKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';

    const res = await gWrapper({
      query: `mutation createCanAccount($name: String!, $ok: String!, $ak: String!) {
        createCanAccount(canAccount: $name, OwnerKey: $ok, ActiveKey: $ak)
      }`,

      variables: {
        name: canAccount,
        ok: ownerKey,
        ak: activeKey
      }
    });

    expect(res).toBeTruthy();
    expect(res.createCanAccount).toBe(true);
  });

  it('querry GraphQl should return false from creating a duplicate account', async () => {
    jest.setTimeout(10000);

    const canAccount = generateCanAccountName();
    const ownerKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';
    const activeKey = 'EOS6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV';

    const res = await gWrapper({
      query: `mutation createCanAccount($name: String!, $ok: String!, $ak: String!) {
        createCanAccount(canAccount: $name, OwnerKey: $ok, ActiveKey: $ak)
      }`,

      variables: {
        name: canAccount,
        ok: ownerKey,
        ak: activeKey
      }
    });

    expect(res).toBeTruthy();
    expect(res.createCanAccount).toBe(true);

    const duplicate_res = await gWrapper({
      query: `mutation createCanAccount($name: String!, $ok: String!, $ak: String!) {
        createCanAccount(canAccount: $name, OwnerKey: $ok, ActiveKey: $ak)
      }`,

      variables: {
        name: canAccount,
        ok: ownerKey,
        ak: activeKey
      }
    });

    expect(duplicate_res).toBeTruthy();
    expect(duplicate_res.createCanAccount).toBe(false);
  });

});